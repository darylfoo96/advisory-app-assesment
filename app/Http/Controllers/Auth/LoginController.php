<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {

            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);

        }

        if ($this->attemptLogin($request)) {

            $user = User::where('email', $request->email)->first();

            // dd($user);

            if ($user->type === 'a') {

                return $this->sendLoginResponse($request);

            }

            $this->guard()->logout();

            $request->session()->invalidate();

            return redirect('/login')->with('error', 'User is not an admin');

        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function appLogin(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if(empty($user)) {

            return response()->json(['status' => ['code' => 400, "message" => 'Invalid Email']]);
        }

        if(!Hash::check($request->password, $user->encrypted_password)) {

            return response()->json(['status' => ['code' => 400, "message" => 'Invalid Password']]);
        }

        $user->token = str_random(32);

        $user->save();

        return response()->json(['id' => $user->id, 'token' => $user->token, 'status' => ['code' => 200, 'message' => 'Access Granted']]);
    }
}
