<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Listing;
use App\Models\User;
use Auth;

class ListingController extends Controller
{
    public function index()
    {
        $listings = Listing::with(['user'])->get();

        return view('listing.index')->with('listings', $listings);
    }

    public function create()
    {
        $users = User::all();

        return view('listing.fields')->with('users', $users);
    }

    public function store(Request $request)
    {
        $listing = new Listing;

        $listing->list_name = $request->name;

        $listing->distance = $request->distance;

        // $listing->user_id = $request->user;

        $listing->user_id = Auth::id();

        $listing->save();

        return redirect('/listing');
    }

    public function edit($id)
    {
        $listing = Listing::find($id);

        $users = User::all();

        return view('listing.fields')->with('listing', $listing)
                                     ->with('users', $users);
    }

    public function update($id, Request $request)
    {
        $listing = Listing::find($id);

        $listing->list_name = $request->name;

        $listing->distance = $request->distance;

        // $listing->user_id = $request->user;

        $listing->user_id = Auth::id();

        $listing->save();

        return redirect('/listing');
    }

    public function destroy($id)
    {
        $listing = Listing::find($id);

        $listing->delete();

        return redirect('/listing');
    }

    public function getListing(Request $request)
    {
        $userToken = User::where('token', $request->token)->first();

        if(empty($userToken)) {

            return response()->json(['status' => ['code' => 400, "message" => 'Invalid token']]);
        }

        $user = User::find($request->id);

        if(empty($user)) {

            return response()->json(['status' => ['code' => 400, "message" => 'Invalid User']]);
        }

        $listings = Listing::where('user_id', $user->id)->get()->toJson();

        return response()->json(['listing' => json_decode($listings), 'status' => ['code' => 200, "message" => 'Listing successfully retrieved']]);
    }

    public function updateListing(Request $request)
    {
        $user = User::where('token', $request->token)->first();

        if(empty($user)) {

            return response()->json(['status' => ['code' => 400, "message" => 'Invalid token']]);
        }

        $listing = Listing::find($request->id);

        if(empty($listing)) {

            return response()->json(['status' => ['code' => 400, "message" => 'Invalid listing']]);
        }

        if(!empty($request->list_name)) {

            $listing->list_name = $request->list_name;
        }

        if(!empty($request->distance)) {

            $listing->distance = $request->distance;
        }

        $listing->user_id = $user->id;

        $listing->save();

        return response()->json(['listing' => json_decode($listing->toJson()), 'status' => ['code' => 200, "message" => 'Listing successfully updated']]);
    }
}
