@extends('layouts.app')

@section('content')
<div class="container">
    @if (!empty($listing))
        {{-- {!! Form::model($listing, ['route' => ['listing.update', $listing->id], 'method' => 'patch']) !!} --}}
        <form action="{{ route('listing.update', $listing->id) }}" method="POST">
            {{ method_field('PATCH') }}
    @else
        <form action="{{ route('listing.store') }}" method="POST">
    @endif
        @csrf
        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('List Name') }}</label>

            <div class="col-md-6">
                <input id="name" type="text" class="form-control" name="name" value="{{ $listing->list_name ?? '' }}" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="distance" class="col-md-4 col-form-label text-md-right">{{ __('Distance') }}</label>

            <div class="col-md-6">
                <input id="distance" type="number" step="0.01" class="form-control" name="distance" value="{{ $listing->distance ?? '' }}" required>
            </div>
        </div>

        {{-- <div class="form-group row">
            <label for="user" class="col-md-4 col-form-label text-md-right">{{ __('User') }}</label>

            <div class="col-md-6">
                <select name="user" id="user" class="form-control">
                    @foreach ($users as $user)
                        @if (!empty($listing))
                            @if ($user->id === $listing->user_id)
                                <option value="{{ $user->id }}" selected>{{ $user->email }}</option>
                            @else
                                <option value="{{ $user->id }}">{{ $user->email }}</option>
                            @endif
                        @else
                            <option value="{{ $user->id }}">{{ $user->email }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div> --}}

        <div class="form-group row mb-0">
            <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Submit') }}
                </button>
            </div>
        </div>
    </form>
</div>
@endsection
