@extends('layouts.app')

@section('content')
<div class="container">
    <a href="{{ route('listing.create') }}" type="button" class="btn">Add New</a>
    <table class="table table-bordered" style="margin-top:20px;">
        <thead>
            <th>Name</th>
            <th>Distance</th>
            <th>User</th>
            <th>Actions</th>
        </thead>
        <tbody>
            @if (!empty($listings))
                @foreach ($listings as $listing)
                    <tr>
                        <td>{{ $listing->list_name }}</td>
                        <td>{{ $listing->distance }}</td>
                        <td>{{ $listing->user->email }} (ID: {{ $listing->user->id }})</td>
                        <td>
                            <form method="POST" action="{{ route('listing.destroy', $listing->id) }}" accept-charset="UTF-8">
                                {{ method_field('DELETE') }}
                                @csrf
                                <a href="{{ route('listing.edit', $listing->id) }}" class='btn btn-success btn-xs'>Edit</a>
                                <input name="_method" type="hidden" value="DELETE">
                                <button style="" type="submit" onclick="return confirm('Are you sure?')" class='btn btn-danger btn-xs'>
                                    Delete
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>
@endsection